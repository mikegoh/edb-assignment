[README.md](https://gitlab.com/mikegoh/edb-assignment) - You are reading it!
# WordPress Docker
The assignment is to provision a WordPress instance on a freshly setup Ubuntu VM on AWS.

[WordPress](https://edbwp.devcubeio.com)  
[PhpMyAdmin](https://edbpma.devcubeio.com)  
[Prometheus](https://edbprom.devcubeio.com)  

## Steps
1. Load mobaXterm (SSH client) with RSA private key
2. sudo apt-get update & sudo apt-get upgrade
3. `timedatectl set-timezone Asia/Singapore` - set SG timezone
4. Install Docker - https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04
5. Install docker-compose (https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04)
6. Create docker-compose.yml
7. Cloudflare DNS - Add CNAME record
```
CNAME edbwp DNS ec2-54-251-65-xx.ap-southeast-1.compute.amazonaws.com
CNAME edbpma DNS ec2-54-251-65-xx.ap-southeast-1.compute.amazonaws.com
```
8. $ wordpress/docker-compose up -d


### Fix WordPress database connection
`docker exec -it <container name> /bin/bash`
```
/** The name of the database for WordPress */
define( 'DB_NAME', getenv_docker('WORDPRESS_DB_NAME', 'wordpress') );

/** Database username */
define( 'DB_USER', getenv_docker('WORDPRESS_DB_USER', 'shxxg-mxi') );

/** Database password */
define( 'DB_PASSWORD', getenv_docker('WORDPRESS_DB_PASSWORD', 'secret') );
```

### Add MySQL user to DB container
```
$ docker exec -it db /bin/bash
CREATE USER 'shxxg-mxi'@'localhost' IDENTIFIED BY 'secret';
GRANT ALL PRIVILEGES ON wordpress.* TO 'shxxg-mxi'@'localhost';
FLUSH PRIVILEGES;
```

## Monitoring
Together with WordPress, please include the necessary monitoring services and tweaks that you will want to have should you be supporting and responsible for the production usage of this WordPress instance.

**Solution**  
Setup Prometheus and it's Alert Manager. 
The Alert Manager will send out alerts based on threshold breached in alert.rules 
Install Node Exporter to gather more performance metrics. 
Grafana is not required as it is a visual overlay. 

### alert.rules - examples
```
# Alert for any instance that is unreachable for >3 minutes
  - alert: instanceDown
    expr: up{job="node-exporter"} == 0
    #expr: up == 0
    for: 3m

# rate(node_network_receive_bytes_total[1m]) = The average network traffic received, per second, over the last minute (in bytes)
  - alert: HostUnusualNetworkThroughputIn
    expr: sum by (instance) (rate(node_network_receive_bytes_total[2m])) / 1024 / 1024 > 100
    for: 5m
```

### Hard Metrics to Monitor
1. Average CPU load over 15 min > 90%
2. Free memory over 15 min < 10%
3. Free diskspace < 15%
4. Bandwidth IN/OUT > X MB (to detect DDOS attack)

### Soft Metrics to Monitor
1. process/service checker (e.g., ps aux | grep apache2) 
2. curl URL for HTTP reponse code (200)

### Install Node Exporter for Prometheus
Don't work since port 9100 closed but can curl localhost:9100
```
$ wget https://github.com/prometheus/node_exporter/releases/download/v1.6.0/node_exporter-1.6.0.linux-amd64.tar.gz
$ tar xvfz node_exporter-1.6.0.linux-amd64.tar.gz

$ sudo mv node_exporter-*/node_exporter /usr/local/bin/
$ sudo useradd -rs /bin/false node_exporter
$ sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
```
**Node Exporter CPU Metrics from SSH console: curl localhost**  
```
shxxg-mxi@ip-100-112-xxx-xx:~/EDB_assignment$ curl http://localhost:9100/metrics | grep cpu
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0# HELP node_cpu_guest_seconds_total Seconds the CPUs spent in guests (VMs) for each mode.
# TYPE node_cpu_guest_seconds_total counter
node_cpu_guest_seconds_total{cpu="0",mode="nice"} 0
node_cpu_guest_seconds_total{cpu="0",mode="user"} 0
# HELP node_cpu_seconds_total Seconds the CPUs spent in each mode.
# TYPE node_cpu_seconds_total counter
node_cpu_seconds_total{cpu="0",mode="idle"} 17684.62
node_cpu_seconds_total{cpu="0",mode="iowait"} 59.12
node_cpu_seconds_total{cpu="0",mode="irq"} 0
node_cpu_seconds_total{cpu="0",mode="nice"} 0
node_cpu_seconds_total{cpu="0",mode="softirq"} 1.27
node_cpu_seconds_total{cpu="0",mode="steal"} 20.65
node_cpu_seconds_total{cpu="0",mode="system"} 92.9
node_cpu_seconds_total{cpu="0",mode="user"} 231.73
```

## Security
From server to application (WordPress) perspective.  

### Server / Web server
**Open ssh**  
```
$ vi /etc/ssh/sshd_config
Port 22522 # don't use default 22
PermitRootLogin no # hackers cannot use 'root' as username
```

**Firewall - AWS Security Group**  
If the instance is from AWS, we can configure the firewall rules form Security Group.
This is prefered over the iptables in the instance itself as threats should be blocked by the rules from security group foremost.
Beside open allowed ports, we can also define range of IPs that is allowed to connect to the EC2.
If the VM is not from any cloud providers, we can fall back to iptables (below)

**Firewall - iptables**  
Follow the "deny all traffic by default,  unless traffic matches explicitly defined rules" 
Flush all rules  
All routes DROP by default  
Specify which ports to open for connection 
Port 22, 80, 443 are common SSH / web service ports
```
# Flush all existing rules
iptables -F

# Set 'close all ports' chain policies
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP

# Accept Established and Related incoming connections
iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT

# Accept all incoming SSH
iptables -A INPUT -p tcp --dport 22 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Accept all incoming HTTP
iptables -A INPUT -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Accept all incoming HTTPS
iptables -A INPUT -p tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Accept incoming PING
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT

# Accept loopback access
iptables -A INPUT -i lo -j ACCEPT
```

**SSL website**  
https://www.ssllabs.com/ssltest/  
Website can be easily secured by an SSL cert but we can further tweak the SSL settings to harden the SSL shield.
```
$ cat /etc/nginx/nginx.conf
##
# SSL Settings
##

ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
ssl_prefer_server_ciphers on;
```

### Application
**Directory Permissions**  
Usually, only the 'public_html' folder need to be exposed and others should retain 644
https://www.malcare.com/blog/wordpress-file-permissions/
```
Root directory (usually public_html): 755
wp-admin: 755
wp-includes: 755
wp-content: 755
wp-content/themes: 755
wp-content/plugins: 755
wp-content/uploads: 755
.htaccess: 644
index.php: 644
wp-config.php: 640
```

**WordPress Security Plugins**  
https://www.bluehost.com/blog/best-wordpress-security-plugins  
Plugins are easily to click and install. They may block brute force log in attempts and provide CAPTCHA to prove human is accessing the website.

**WordPress Database Plugins**  
Overtime, there are redundant image files and temporary data stored in the database. Such 'cleaner' plugins have the ability to scan and delete these files or data permanently. Usually, it saves some disk space and reduce the database size a little.

**.env**  
Store secrets in .env files and not in the actual yaml files or scripts itself.

## Backup
**mysqldump.sh**  
This bash script will connect into the db container and mysqldump the wordpress database to local storage.
For housekeeping, it will delete files older than > 21 days
This is useful if we did irreversible damage to production database and can recover quickly locally.
Ideally, backups should also be stored remotely as well. E.g., via Linux command: rsync

**monitor-url.sh / monitor-url.py**  
Both script will curl the application website for HTTP status code.
If not return '200', send email to alert owner.

## Other Tweaks
Deploy distributed memory object caching system to improve web serving experience. 
Memcached and redis are popular ones.  

Setup postfix or exim mail service so that cron scripts can send out alert emails.

**AWS end**  
- Vertical scaling in AWS whereby the EC2 will scale up or down depending on needs.
- Horizontal scaling in AWS whereby a X number of EC2 will spun up or down depending on needs.
- Deploy Amazon CloudFront to deliver static assets (such as videos, images, and files) with low latency.
- Use Elastic Load Balancing (ELB) to distribute incoming application traffic across multiple targets and ideally across one or more Availability Zones (AZs) but having said that, need to consider price vs availability.

## Tools & Skills Demonstrated
- Linux adminstration 
- Basic server hardening (SSH, firewall)
- Docker containers (Docker Compose)
- Setting up monitoring tools (Prometheus/ alerts, Node Exporter metrics)
- Coding in Bash, Python and YAML scripts (Backup DB, curl URL)
- Cloud knowledge in AWS (Auto Scaling, CloudFront)
- Troubleshooting 500,502 HTTP errors
- MySQL administration (Add user, Grant privileges)
- Deploy SSL certs (Let's Encrypt) and Nginx Proxy (jwilder/nginx-proxy)
- Git push, commit (this README.md)
- DNS records (Add CNAME in Cloudflare)
